class CartManager:
    """
    Cart manager
    This class use to store owner's first name, last name and items.
    """
    
    def __init__(self, first_name: str, last_name: str):
        """
        Initialize (Constructor).
        Arguments:
            first_name -- A first name of owner (eg. 'Benjapol', 'Kasidit' )
            last_name -- A last name of owner (eg. 'Worakan', 'Phoncharoen' )
        """
        self.FIRST_NAME = first_name
        self.LAST_NAME = last_name
        self.item = {}
        

    def add_item(self, item: str, amount: int):
        """
        Add a item to items.
        Arguments:
            item(String) -- An item as string (eg. 'egg', 'milk').
            amount(int)  -- An amount of item (eg. 3, 2)
        """
        self.item[item] = self.item.get(item,0)+amount

    def remove_item(self, item):
        """
        Remove a item from items.
        Arguments:
            item -- An item as string (eg. 'egg', 'milk').
        """
        self.item.pop(item, 0)

    @property
    def total_amount_items(self) -> int:
        """
        Returns total number of items in cart
        """
        return sum(self.item.values())

    @property
    def owner_name(self) -> dict:
        """
        Return owner's first name and last name in cart as Dict
        """
        return {'first_name': self.FIRST_NAME , 'last_name': self.LAST_NAME}
        

    @property
    def items(self) -> dict:
        """
        Return items in cart
        """
        return self.item
